#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>
#include <sys/file.h>
#include "mruby.h"
#include "mruby/class.h"
#include "mruby/value.h"
#include "mruby/variable.h"
#include "mruby/data.h"
#include "mruby/range.h"
#include "mruby/string.h"

struct mmap {
  size_t  off;
  size_t  siz;
  struct mmap_inner {
    void   *buf;
    size_t  len;
    int     fd;
    int     cnt;
  }      *mi;
};

static void
mrb_mmap_free(mrb_state *mrb, void *ptr)
{
  struct mmap *m = ptr;

  if (m) {
    if (m->mi) {
      if (--m->mi->cnt <= 0) {
        if (m->mi->fd >= 0) close(m->mi->fd);
        if (m->mi->buf != NULL) munmap(m->mi->buf, m->mi->len);
        mrb_free(mrb, m->mi);
      }
    } 
    mrb_free(mrb, m);
  }
}

const static struct mrb_data_type mrb_mmap_type = { "Mmap", &mrb_mmap_free };

mrb_value
mrb_mmap_init(mrb_state *mrb, mrb_value self)
{
  struct mmap *m;
  char          *filename;
  mrb_int        size;
  mrb_int        argc;
  struct stat    st;

  if (DATA_PTR(self)) {
    DATA_TYPE(self)->dfree(mrb, DATA_PTR(self));
    DATA_PTR(self) = NULL;
  }

  m      = (struct mmap *) mrb_malloc(mrb, sizeof(struct mmap));
  m->off = 0;
  m->siz = 0;
  m->mi  = NULL;

  m->mi      = (struct mmap_inner *) mrb_malloc(mrb, sizeof(struct mmap_inner));
  m->mi->buf = NULL;
  m->mi->len = 0;
  m->mi->cnt = 1;
  m->mi->fd  = -1;

  DATA_PTR(self)  = m;
  DATA_TYPE(self) = &mrb_mmap_type;

  argc = mrb_get_args(mrb, "z|i", &filename, &size);
  if (argc >= 1 && size <= 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "size must be positive integer");
  }
  m->mi->fd = open(filename, O_RDWR);
  if (m->mi->fd < 0) {
    mrb_raise(mrb, E_RUNTIME_ERROR, strerror(errno));
  }
  if (fstat(m->mi->fd, &st) < 0) {
    mrb_raise(mrb, E_RUNTIME_ERROR, strerror(errno));
  }
  if (argc == 1) {
    size = st.st_size;
  }
  if (size > st.st_size) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "size must be less than or equal filesize");
  }
  
  m->mi->buf = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, m->mi->fd, 0);
  m->mi->len = size;
  if (m->mi->buf == MAP_FAILED)
  {
    m->mi->buf = NULL;
    mrb_raise(mrb, E_RUNTIME_ERROR, strerror(errno));
  }

  m->off = 0;
  m->siz = size;
  return self;
}

mrb_value
mrb_mmap_size(mrb_state *mrb, mrb_value self)
{
  struct mmap * m = DATA_PTR(self);
  return mrb_fixnum_value(m->siz);
}

mrb_value
mrb_mmap_init_copy(mrb_state *mrb, mrb_value copy)
{
  struct mmap *m;
  struct mmap *msrc;
  mrb_value src;
  
  mrb_get_args(mrb, "o", &src);
  if (mrb_obj_equal(mrb, copy, src)) return copy;
  if (!mrb_obj_is_instance_of(mrb, src, mrb_obj_class(mrb, copy))) {
    mrb_raise(mrb, E_TYPE_ERROR, "wrong argument class");
  }
  if (DATA_PTR(copy)) {
    DATA_TYPE(copy)->dfree(mrb, DATA_PTR(copy));
    DATA_PTR(copy) = NULL;
  }
  DATA_PTR(copy)  = mrb_malloc(mrb, sizeof(struct mmap));
  DATA_TYPE(copy) = &mrb_mmap_type;
  m    = DATA_PTR(copy);
  msrc = DATA_PTR(src);
  *m = *msrc;
  m->mi->cnt ++;
  return copy;
}

mrb_value
mrb_mmap_to_s(mrb_state *mrb, mrb_value self)
{
  struct mmap *m = DATA_PTR(self);
  mrb_value      ret;
  
  flock(m->mi->fd, LOCK_SH);
  ret = mrb_str_new(mrb, m->mi->buf + m->off, m->siz);
  flock(m->mi->fd, LOCK_UN);
  return ret;
}

mrb_value
mrb_mmap_sub(mrb_state *mrb, mrb_value self, mrb_int beg, mrb_int len)
{
  struct mmap *m = DATA_PTR(self);
  mrb_value    sub;
  struct mmap *msub;

  if (beg < 0) beg += m->siz;
  if (beg < 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "negative offset");
  }
  if (len <= 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "zero or negative size");
  }
  if (beg >= m->siz) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "offset exceeded");
  }
  if (len > m->siz - beg) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "size exceeded");
  }

  sub            = mrb_obj_value(mrb_obj_alloc(mrb, MRB_TT_DATA, mrb_obj_class(mrb, self)));
  msub           = mrb_malloc(mrb, sizeof(struct mmap));
  *msub          = *m;
  msub->off      = m->off + beg;
  msub->siz      = len;
  msub->mi->cnt ++;
  DATA_PTR(sub)  = msub;
  DATA_TYPE(sub) = &mrb_mmap_type;
  return sub;
}

mrb_value
mrb_mmap_msub(mrb_state *mrb, mrb_value self)
{
  struct mmap *m = DATA_PTR(self);
  mrb_int beg;
  mrb_int len;
  mrb_int argc;

  argc = mrb_get_args(mrb, "i|i", &beg, &len);
  if (argc == 1) {
    if (beg < 0) beg += m->siz;
    len = m->siz - beg;
  }
  return mrb_mmap_sub(mrb, self, beg, len);
}

mrb_value
mrb_mmap_mget(mrb_state *mrb, mrb_value self)
{
  struct mmap *m = DATA_PTR(self);
  mrb_int      argc;
  mrb_value    idx;
  mrb_int      beg;
  mrb_int      len;
  
  argc = mrb_get_args(mrb, "o|i", &idx, &len);
  switch (mrb_type(idx)) {

  case MRB_TT_RANGE:
    if (argc > 1) {
      mrb_raise(mrb, E_ARGUMENT_ERROR, "invalid argument for []");
    }
    len = m->siz;
    if (mrb_range_beg_len(mrb, idx, &beg, &len, len, TRUE) == 1) {
      return mrb_mmap_sub(mrb, self, beg, len);
    }
    return mrb_nil_value();

  case MRB_TT_FIXNUM:
    argc = mrb_get_args(mrb, "i|i", &beg, &len);
    if (argc == 1) len = 1;
    return mrb_mmap_sub(mrb, self, beg, len);

  default:
    mrb_raise(mrb, E_ARGUMENT_ERROR, "invalid argument for []");
  }

}

static void
mrb_mmap_copy(mrb_state *mrb, struct mmap *mdst, mrb_int beg, void *srcbuf, size_t siz)
{
  void *dstbuf = mdst->mi->buf + mdst->off + beg;

  if (dstbuf == srcbuf) return;

  flock(mdst->mi->fd, LOCK_EX);
  if (dstbuf + siz <= srcbuf || srcbuf + siz <= dstbuf) {
    memcpy(dstbuf, srcbuf, siz);
  } else {
    memmove(dstbuf, srcbuf, siz);
  }
  flock(mdst->mi->fd, LOCK_UN);
}

static void
mrb_mmap_copy_check(mrb_state *mrb, struct mmap *mdst, mrb_int beg, mrb_int len)
{
  if (beg < 0) beg += mdst->siz;
  if (beg < 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "negative offset");
  }
  if (len <= 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "zero or negative size");
  }
  if (beg >= mdst->siz) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "offset exceeded");
  }
  if (len > mdst->siz - beg) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "size exceeded");
  }
}

static void
mrb_mmap_copy_mmap(mrb_state *mrb, struct mmap *mdst, mrb_int beg, struct mmap *msrc)
{
  mrb_mmap_copy_check(mrb, mdst, beg, msrc->siz);

  flock(msrc->mi->fd, LOCK_SH);
  mrb_mmap_copy(mrb, mdst, beg, msrc->mi->buf + msrc->off, msrc->siz);
  flock(msrc->mi->fd, LOCK_UN);
}

static void
mrb_mmap_subset(mrb_state *mrb, struct mmap *mdst, mrb_int beg, mrb_int len, mrb_value src)
{
  if (mrb_type(src) == MRB_TT_DATA && DATA_TYPE(src) && strcmp("Mmap", DATA_TYPE(src)->struct_name) == 0) {
    struct mmap *msrc = DATA_PTR(src);

    if (len != msrc->siz) {
      mrb_raise(mrb, E_ARGUMENT_ERROR, "size no match");
    }
    mrb_mmap_copy_mmap(mrb, mdst, beg, msrc);
  }
  else if (mrb_string_p(src)) {
    char    *buf = RSTRING_PTR(src);
    mrb_int  siz = RSTRING_LEN(src);

    if (len != siz) {
      mrb_raise(mrb, E_ARGUMENT_ERROR, "size no match");
    }
    mrb_mmap_copy_check(mrb, mdst, beg, len);
    mrb_mmap_copy(mrb, mdst, beg, buf, len);
  }
  else {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "invalid assignment");
  }
}

mrb_value
mrb_mmap_mset(mrb_state *mrb, mrb_value self)
{
  struct mmap *m = DATA_PTR(self);
  mrb_value    fst;
  mrb_value    snd;
  mrb_value    thd;
  mrb_int      beg;
  mrb_int      len;
  mrb_int      argc;

  argc = mrb_get_args(mrb, "oo|o", &fst, &snd, &thd);
  switch (mrb_type(fst)) {
  case MRB_TT_RANGE:
    if (argc > 2) {
      mrb_raise(mrb, E_ARGUMENT_ERROR, "invalid argument");
    }
    len = m->siz;
    if (mrb_range_beg_len(mrb, fst, &beg, &len, len, FALSE) == 1) {
      mrb_mmap_subset(mrb, m, beg, len, snd);
      return self;
    }
    else {
      mrb_raise(mrb, E_ARGUMENT_ERROR, "invalid argument");
    }
  case MRB_TT_FIXNUM:
    switch (argc) {
    case 2:
      argc = mrb_get_args(mrb, "io", &beg, &snd);
      mrb_mmap_subset(mrb, m, beg, 1, snd);
      return self;
    case 3:
      argc = mrb_get_args(mrb, "iio", &beg, &len, &thd);
      mrb_mmap_subset(mrb, m, beg, len, thd);
      return self;
    }
    mrb_assert(0);
  default:
    mrb_raise(mrb, E_ARGUMENT_ERROR, "invalid argument");
  }
}

static void
mrb_mmap_flock(mrb_state *mrb, mrb_value self, int operation)
{
  struct mmap *m = DATA_PTR(self);

  if (flock(m->mi->fd, operation) != 0) {
    mrb_raise(mrb, E_RUNTIME_ERROR, strerror(errno));
  }
}

mrb_value
mrb_mmap_flock_sh(mrb_state *mrb, mrb_value self)
{
  mrb_mmap_flock(mrb, self, LOCK_SH);
  return mrb_nil_value();
}

mrb_value
mrb_mmap_flock_ex(mrb_state *mrb, mrb_value self)
{
  mrb_mmap_flock(mrb, self, LOCK_EX);
  return mrb_nil_value();
}

mrb_value
mrb_mmap_flock_un(mrb_state *mrb, mrb_value self)
{
  mrb_mmap_flock(mrb, self, LOCK_UN);
  return mrb_nil_value();
}

mrb_value
mrb_mmap_filesize(mrb_state *mrb, mrb_value self)
{
  struct mmap *m = DATA_PTR(self);
  struct stat st;

  if (fstat(m->mi->fd, &st) < 0) {
    mrb_raise(mrb, E_RUNTIME_ERROR, strerror(errno));
  }
  return mrb_fixnum_value(st.st_size);
}

mrb_value
mrb_mmap_filesize_s(mrb_state *mrb, mrb_value self)
{
  char *filename;
  struct stat st;

  mrb_get_args(mrb, "z", &filename);
  if (stat(filename, &st) < 0) {
    mrb_raise(mrb, E_RUNTIME_ERROR, strerror(errno));
  }
  return mrb_fixnum_value(st.st_size);
}

mrb_value
mrb_mmap_get_f(mrb_state *mrb, mrb_value self)
{
  struct mmap *m = DATA_PTR(self);
  mrb_int      beg;
  mrb_float    f;

  mrb_get_args(mrb, "i", &beg);
  if (beg < 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "negative index");
  }
  if (beg + sizeof(mrb_float) > m->siz) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "out of bound index");
  }
  flock(m->mi->fd, LOCK_SH);
  f = *(mrb_float *) (m->mi->buf + m->off + beg);
  flock(m->mi->fd, LOCK_UN);
  return mrb_float_value(mrb, f);
}

mrb_value
mrb_mmap_set_f(mrb_state *mrb, mrb_value self)
{
  struct mmap *m = DATA_PTR(self);
  mrb_int      beg;
  mrb_float    f;

  mrb_get_args(mrb, "if", &beg, &f);
  if (beg < 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "negative index");
  }
  if (beg + sizeof(mrb_float) > m->siz) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "out of bound index");
  }
  flock(m->mi->fd, LOCK_EX);
  *(mrb_float *) (m->mi->buf + m->off + beg) = f;
  flock(m->mi->fd, LOCK_UN);
  return mrb_nil_value();
}

mrb_value
mrb_mmap_get_i(mrb_state *mrb, mrb_value self)
{
  struct mmap *m = DATA_PTR(self);
  mrb_int      beg;
  mrb_int      i;

  mrb_get_args(mrb, "i", &beg);
  if (beg < 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "negative index");
  }
  if (beg + sizeof(mrb_float) > m->siz) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "out of bound index");
  }
  flock(m->mi->fd, LOCK_SH);
  i = *(mrb_int *) (m->mi->buf + m->off + beg);
  flock(m->mi->fd, LOCK_UN);
  return mrb_fixnum_value(i);
}

mrb_value
mrb_mmap_set_i(mrb_state *mrb, mrb_value self)
{
  struct mmap *m = DATA_PTR(self);
  mrb_int      beg;
  mrb_int      i;

  mrb_get_args(mrb, "ii", &beg, &i);
  if (beg < 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "negative index");
  }
  if (beg + sizeof(mrb_float) > m->siz) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "out of bound index");
  }
  flock(m->mi->fd, LOCK_EX);
  *(mrb_int *) (m->mi->buf + m->off + beg) = i;
  flock(m->mi->fd, LOCK_UN);
  return mrb_nil_value();
}

mrb_value
mrb_mmap_get_b(mrb_state *mrb, mrb_value self)
{
  struct mmap  *m = DATA_PTR(self);
  mrb_int       beg;
  int           bit;
  unsigned char dat;

  mrb_get_args(mrb, "i", &beg);
  if (beg < 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "negative index");
  }
  bit = beg & 7;
  beg >>= 3;
  if (beg > m->siz) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "out of bound index");
  }
  flock(m->mi->fd, LOCK_SH);
  dat = *(unsigned char *) (m->mi->buf + m->off + beg);
  flock(m->mi->fd, LOCK_UN);

  return (dat & 1 << bit) ? mrb_true_value() : mrb_false_value();
}

mrb_value
mrb_mmap_set_b(mrb_state *mrb, mrb_value self)
{
  struct mmap *m = DATA_PTR(self);
  mrb_int      beg;
  int          bit;
  mrb_bool     b;

  mrb_get_args(mrb, "ib", &beg, &b);
  if (beg < 0) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "negative index");
  }
  bit = beg & 7;
  beg >>= 3;
  if (beg + sizeof(mrb_float) > m->siz) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "out of bound index");
  }
  flock(m->mi->fd, LOCK_EX);
  if (b) {
    *(unsigned char *) (m->mi->buf + m->off + beg) |= 1 << bit;
  }
  else {
    *(unsigned char *) (m->mi->buf + m->off + beg) &= ~(1 << bit);
  }
  flock(m->mi->fd, LOCK_UN);
  return mrb_nil_value();
}

void
mrb_mruby_mmap_gem_init(mrb_state *mrb)
{
  struct RClass *mmap_cls;

  mmap_cls = mrb_define_class(mrb, "Mmap", mrb->object_class);
  MRB_SET_INSTANCE_TT(mmap_cls, MRB_TT_DATA);
  mrb_define_method      (mrb, mmap_cls, "initialize",      mrb_mmap_init,       MRB_ARGS_ARG(1, 1));
  mrb_define_method      (mrb, mmap_cls, "initialize_copy", mrb_mmap_init_copy,  MRB_ARGS_REQ(1));
  mrb_define_method      (mrb, mmap_cls, "to_s",            mrb_mmap_to_s,       MRB_ARGS_NONE());
  mrb_define_method      (mrb, mmap_cls, "size",            mrb_mmap_size,       MRB_ARGS_NONE());
  mrb_define_method      (mrb, mmap_cls, "sub",             mrb_mmap_msub,       MRB_ARGS_ARG(1, 1));
  mrb_define_method      (mrb, mmap_cls, "[]",              mrb_mmap_mget,       MRB_ARGS_ARG(1, 1));
  mrb_define_method      (mrb, mmap_cls, "[]=",             mrb_mmap_mset,       MRB_ARGS_ARG(1, 1));
  mrb_define_method      (mrb, mmap_cls, "set_f",           mrb_mmap_set_f,      MRB_ARGS_REQ(2));
  mrb_define_method      (mrb, mmap_cls, "get_f",           mrb_mmap_get_f,      MRB_ARGS_REQ(1));
  mrb_define_method      (mrb, mmap_cls, "set_i",           mrb_mmap_set_i,      MRB_ARGS_REQ(2));
  mrb_define_method      (mrb, mmap_cls, "get_i",           mrb_mmap_get_i,      MRB_ARGS_REQ(1));
  mrb_define_method      (mrb, mmap_cls, "set_b",           mrb_mmap_set_b,      MRB_ARGS_REQ(2));
  mrb_define_method      (mrb, mmap_cls, "get_b",           mrb_mmap_get_b,      MRB_ARGS_REQ(1));
  mrb_define_method      (mrb, mmap_cls, "flock_sh",        mrb_mmap_flock_sh,   MRB_ARGS_NONE());
  mrb_define_method      (mrb, mmap_cls, "flock_ex",        mrb_mmap_flock_ex,   MRB_ARGS_NONE());
  mrb_define_method      (mrb, mmap_cls, "flock_un",        mrb_mmap_flock_un,   MRB_ARGS_NONE());
  mrb_define_method      (mrb, mmap_cls, "filesize",        mrb_mmap_filesize,   MRB_ARGS_NONE());
  mrb_define_class_method(mrb, mmap_cls, "filesize",        mrb_mmap_filesize_s, MRB_ARGS_NONE());
  mrb_define_const       (mrb, mmap_cls, "FLOAT_SIZE",      mrb_fixnum_value(sizeof(mrb_float)));
  mrb_define_const       (mrb, mmap_cls, "INT_SIZE",        mrb_fixnum_value(sizeof(mrb_int)));
}

void
mrb_mruby_mmap_gem_final(mrb_state *mrb)
{
}

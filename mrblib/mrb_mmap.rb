class Mmap
  #
  # run block with write lock
  # 
  def do_write(*args)
    flock_ex
    begin
      yield(*args)
    ensure
      flock_un
    end
  end

  #
  # run block with read lock
  #
  def do_read(*args)
    flock_sh
    begin
      yield(*args)
    ensure
      flock_un
    end
  end

  #
  # size of float array
  #
  def size_f
    size.div FLOAT_SIZE
  end

  #
  # size of int array
  #
  def size_i
    size.div INT_SIZE
  end

  #
  # size of bit array
  #
  def size_b
    size * 8
  end
  
  #
  # get float array    when block is not supplied
  # each by float data when block is     supplied
  #
  def floats
    if block_given?
      i = 0
      s = size
      while i + FLOAT_SIZE <= s
        yield get_f(i)
	i += FLOAT_SIZE
      end
    else
      FloatArray.new self
    end
  end

  #
  # get int array    when block is not supplied
  # each by int data when block is     supplied
  #
  def ints
    if block_given?
      i = 0
      s = size
      while i + INT_SIZE <= s
	yield get_i(i)
	i += INT_SIZE
      end
    else
      IntArray.new self
    end
  end

  #
  # get bit array    when block is not supplied
  # each by int data when block is     supplied
  #
  def bits
    if block_given?
      i = 0
      s = size
      while i < s * 8
	yield get_b(i)
	i += 1
      end
    else
      BitArray.new self
    end
  end

  private

  #
  # Base Class of Arrays
  #
  class MmapArray
    include Enumerable

    #
    # Initializer
    #
    def initialize(mmap)
      @mmap = mmap
    end

    #
    # convert to string
    #
    def to_s
      ret = '['
      first = true
      each do |e|
	if first
	  first = false
	else
	  ret += ', '
	end
	ret += e.to_s
      end
      ret += ']'
      ret
    end
  end

  #
  # Float Array
  #
  class FloatArray < MmapArray
    def each
      @mmap.floats { |f| yield f }
    end
    def [](idx)
      @mmap.get_f idx * FLOAT_SIZE  
    end
    def []=(idx, val)
      @mmap.set_f idx * FLOAT_SIZE, val
    end
    def size
      @mmap.size_f
    end
  end

  #
  # Int Array
  #
  class IntArray < MmapArray
    def each
      @mmap.ints { |i| yield i }
    end
    def [](idx)
      @mmap.get_i idx * INT_SIZE  
    end
    def []=(idx, val)
      @mmap.set_i idx * INT_SIZE, val
    end
    def size
      @mmap.size_i
    end
  end

  #
  # Bit Array
  #
  class BitArray < MmapArray
    def each
      @mmap.bits { |i| yield i }
    end
    def [](idx)
      @mmap.get_b idx  
    end
    def []=(idx, val)
      @mmap.set_b idx, val
    end
    def size
      @mmap.size_b
    end
  end

end
